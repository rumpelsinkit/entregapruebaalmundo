package com.almundo.client;

import java.io.IOException;
import java.util.concurrent.Future;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.InvocationCallback;
import javax.ws.rs.client.WebTarget;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

public class ClientDispatcher {

	public static void main(String[] args) throws JsonGenerationException, JsonMappingException, IOException {
		for(int i=1;i<=10;i++){
		     generateRequest(String.valueOf(i));
		}
	}
	
	/**
	 * Este método se encarga de generar una petición Rest asincrona al servicio
	 * asignandole un id para identificarla 
	 * @param id
	 * @throws IOException
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 */
	public static void generateRequest(String id) throws IOException, JsonGenerationException, JsonMappingException {
		Client client = ClientBuilder.newBuilder().build();
		
		LLamada  llmda= new LLamada();
		llmda.setCallId(id);
		ObjectMapper mapper = new ObjectMapper();
		String pa= mapper.writeValueAsString(llmda).replace("{", "%7B").replace("}", "%7D");
		WebTarget target= client.target("http://localhost:8080/CallDispatcher/serviceDispatch/DispatchCall/").path(pa);
	
		Future<String> responseFuture = target
		        .request().async().get(new InvocationCallback<String>() {
		            @Override
		            public void completed(String response) {
		                System.out.println("Response status code "
		                        + response + " received.");
		            }
		 
		            @Override
		            public void failed(Throwable throwable) {
		                System.out.println("Invocation failed.");
		                throwable.printStackTrace();
		            }
		        });
	}
	

}
