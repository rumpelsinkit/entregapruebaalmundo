package co.com.app.service.vo;
/**
 * Clase que representa el mensaje recibido y la respuesta retornada
 * @author legyoroz
 *
 */
public class LLamada {
	private String nombreEmpleado;
	private int duracionLLamada;
	private String mensaje;
	private String callId;
	public LLamada(){
		
	}
	public String getNombreEmpleado() {
		return nombreEmpleado;
	}
	public void setNombreEmpleado(String nombreEmpleado) {
		this.nombreEmpleado = nombreEmpleado;
	}
	public int getDuracionLLamada() {
		return duracionLLamada;
	}
	public void setDuracionLLamada(int duracionLLamada) {
		this.duracionLLamada = duracionLLamada;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getCallId() {
		return callId;
	}
	public void setCallId(String callId) {
		this.callId = callId;
	}
	

}
