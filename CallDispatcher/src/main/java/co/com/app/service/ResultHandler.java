package co.com.app.service;

import org.springframework.web.context.request.async.DeferredResult.DeferredResultHandler;

public class ResultHandler implements DeferredResultHandler {

	private String resultado;
	@Override
	public void handleResult(Object arg0) {
		 
		setResultado((String)arg0);

	}
	public String getResultado() {
		return resultado;
	}
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

}
