package co.com.app.service;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.context.request.async.DeferredResult.DeferredResultHandler;

import co.com.app.facade.IDispatcher;
import co.com.app.service.vo.LLamada;


@Controller
@RequestMapping("/serviceDispatch")
public class CallService {
	
	@Autowired
	public IDispatcher dispatcher;
	/**
	 * Método que expone el servico asincronaqmente recibe un objeto json correspondiente
	 * a la clase LLamada
	 * @param call
	 * @return
	 */
	@RequestMapping(value="/DispatchCall/{call}", method = RequestMethod.GET)
	@ResponseBody
	public DeferredResult<String> dispachCall(@PathVariable String call) {
		DeferredResult<String> deferredResult = new DeferredResult<String>();
		DeferredResultHandler df = new DeferredResultHandler() {

			@Override
			public void handleResult(Object arg0) {

			  System.out.println((String)arg0);
			}
			
		};
		deferredResult.setResultHandler(df);
       
		ObjectMapper mapper = new ObjectMapper();
		LLamada llamada=null;
		LLamada response= null;
		String rsult="";
		try {
			llamada = mapper.readValue(call, LLamada.class);
		} catch (JsonParseException e1) {
			e1.printStackTrace();
		} catch (JsonMappingException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		response=dispatcher.dispatchCall(llamada);
		try {
			rsult = mapper.writeValueAsString(response);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		deferredResult.setResult(rsult);
		return deferredResult;
	}
}
