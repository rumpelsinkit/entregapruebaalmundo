package co.com.app.facade;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.app.business.CallManager;
import co.com.app.dispatchers.Director;
import co.com.app.dispatchers.Empleado;
import co.com.app.dispatchers.Operador;
import co.com.app.dispatchers.Supervisor;
import co.com.app.service.vo.LLamada;
/**
 * Clase que despacha las peticiones asignandolas  a los
 * empleados desponibles
 * @author legyoroz
 *
 */
@Service
public  class Dispatcher implements IDispatcher {
	@Autowired
	public CallManager manager;

	private  Empleado attenders;
    /**
     * Constructor que carga la cadena de responsabilidad
     */
	public Dispatcher(){
		attenders = cargaCadenaResponsabilidad(6,3,1);
	}
	/**
	 * Este método  asigna la llamada a un empleado
	 * en primera instancia a un operador si no hay operadores disponibles 
	 * la asigna a un supervisor en ultima instancia se la asigna a un Director
	 */
	@Override
	public LLamada dispatchCall(LLamada llamada){
		LLamada call = llamada;


		//call = manager.procesarLLamada(buscarDisponible(attenders), llamada);
		call= manager.procesarLLamadaAlt(this, llamada);
		return call;

	}

	public Empleado getAttenders() {
		return attenders;
	}

	public void setAttenders(Empleado attenders) {
		this.attenders = attenders;
	}
	/**
	 * Este método se encarga de bucar empleados disponibles 
	 * para asignar la llamada
	 * @param empl
	 * @return
	 */
	public Empleado buscarDisponible(Empleado empl){
		Empleado emp = null;
		List<Empleado> itatt = empl.getEmpleados();

		for(int i=0;i<itatt.size();i++){
			if(itatt.get(i).getDisponible()){
				itatt.get(i).setDisponible(false);
				return itatt.get(i);
			}
		}
		if(empl.getAsignarEmpleado()!= null){
			return buscarDisponible(empl.getAsignarEmpleado());
		}
		return emp;
	}
	/**
	 * Se encarga de crar la jerarquia de empleados disponibles 
	 * para atender las llamadas
	 * @param operadores
	 * @param supervisores
	 * @param directores
	 * @return
	 */
	public Empleado cargaCadenaResponsabilidad(int operadores,int supervisores,int directores){
		Empleado operador= new Operador();
		List<Empleado> opres = new ArrayList<>();
		List<Empleado> supres = new ArrayList<>();
		List<Empleado> dirtores = new ArrayList<>();
		for(int i=1;i<=operadores;i++){
			Empleado op = new Operador();
			op.setNombreEmpleado(Operador.tipo+i);
			opres.add(op);
		}
		for(int i=1;i<=supervisores;i++){
			Empleado sup = new Supervisor();
			sup.setNombreEmpleado(Supervisor.tipo+i);
			supres.add(sup);
		}
		for(int j=1;j<=directores;j++){
			Empleado dir = new Director();
			dir.setNombreEmpleado(Director.tipo+j);
			dirtores.add(dir);
		}
		operador.setEmpleados(opres);
		Empleado supervis = new Supervisor();
		supervis.setEmpleados(supres);
		Empleado direct = new Director();
		direct.setEmpleados(dirtores);
		supervis.setAsignarEmpleado(direct);
		operador.setAsignarEmpleado(supervis);
		return operador;
	}

}
