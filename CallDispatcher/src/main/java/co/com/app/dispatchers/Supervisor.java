package co.com.app.dispatchers;

import co.com.app.business.ProcesoLLamada;
import co.com.app.service.vo.LLamada;

public class Supervisor extends Empleado{
    public static String tipo="supervisor";
	@Override
	public ProcesoLLamada atenderLLamada(Empleado empl,LLamada llamada) {
		ProcesoLLamada process = new ProcesoLLamada(empl,llamada);
		return process;
	}



}
