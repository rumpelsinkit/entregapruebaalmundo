package co.com.app.business;

import java.util.Random;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import co.com.app.dispatchers.Empleado;
import co.com.app.facade.Dispatcher;
import co.com.app.facade.IDispatcher;
import co.com.app.service.vo.LLamada;
/**
 * Clase manager establece el pool de hilos para atender las peticiones
 * @author legyoroz
 *
 */
@Service
public class CallManager {
	/***
	 * pool de hilos que procesa la peticones
	 */
	@Autowired
	public ThreadPoolTaskExecutor  threadPoolTaskExecutor;


	public CallManager(){

	}
	/**
	 * Este método se encargar de asignar el proceso de la llamada
	 * @param empl
	 * @param call
	 * @return
	 */
	public LLamada procesarLLamada(Empleado empl,LLamada call){
		LLamada llamada = call; 
		llamada.setDuracionLLamada(generateDelay());
		if(empl != null){
			ProcesoLLamada pro = empl.atenderLLamada(empl,llamada);
			try{
				threadPoolTaskExecutor.setRejectedExecutionHandler(new RejectedExecutionHandler() {

					@Override
					public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
						System.out.println(((ProcesoLLamada)r).getIdllamada()+"error");

					}
				});
				threadPoolTaskExecutor.submit(empl.atenderLLamada(empl,llamada));
				
			}catch(Exception e){
				System.out.println("Excepcion atender llamada" +e.getMessage());
				
			}
			llamada.setNombreEmpleado(empl.getNombreEmpleado());
		}else{
			llamada.setMensaje("No hay empleados disponibles");

		}
		return llamada;
	}
	
	public LLamada procesarLLamadaAlt(Dispatcher disp,LLamada call){
		LLamada llamada = call; 
		llamada.setDuracionLLamada(generateDelay());
		
			ProcesoLLamada pro = new ProcesoLLamada(disp, llamada);
			try{
				threadPoolTaskExecutor.setRejectedExecutionHandler(new RejectedExecutionHandler() {

					@Override
					public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
						System.out.println(((ProcesoLLamada)r).getIdllamada()+"error");

					}
				});
				threadPoolTaskExecutor.submit(pro);
				System.out.println("server request:"+llamada.getCallId());
				
			}catch(Exception e){
				System.out.println("Excepcion atender llamada" +e.getMessage());
				
			}
		
		return llamada;
	}
	
	/**
	 * Método que gera valores aleatoriso en un rango para asignar un delay 
	 * de cada proceso asumiendos como la duracion de la llamada
	 * @return
	 */
	public int generateDelay(){
		Random generador = new Random();
		int delay = 5000+generador.nextInt(10*1000);

		return delay;
	}
}
