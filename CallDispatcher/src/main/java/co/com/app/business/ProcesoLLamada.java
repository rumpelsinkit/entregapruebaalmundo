package co.com.app.business;

import co.com.app.dispatchers.Empleado;
import co.com.app.facade.Dispatcher;
import co.com.app.facade.IDispatcher;
import co.com.app.service.vo.LLamada;
/**
 * Clase que implementa la atención de un empleado a una llamada
 * @author legyoroz
 *
 */
public class ProcesoLLamada implements Runnable {
    
	private int delay;
	private Empleado empl;
	private int idllamada;
    private Dispatcher dispatcher;
	
	/**
	 * Constructor de la clase establece los valores iniciales 
	 * los cuales sirven para visualizar la ejecución y salidas de los
	 * hilos generados
	 * @param empl
	 * @param llamada
	 */
	public ProcesoLLamada(Empleado empl,LLamada llamada) {
		delay = llamada.getDuracionLLamada();
		this.empl = empl;
		idllamada = Integer.parseInt(llamada.getCallId());
	}
	
	public ProcesoLLamada(Dispatcher disp,LLamada llamada) {
		delay = llamada.getDuracionLLamada();
		idllamada = Integer.parseInt(llamada.getCallId());
		this.dispatcher =disp;
		this.empl= disp.buscarDisponible(disp.getAttenders());
		llamada.setNombreEmpleado(empl.getNombreEmpleado());
		
	}
	/**
	 * implementa la accion que realiza el empleado 
	 */
	@Override
	public void run() {
		if(this.empl== null){
			this.empl =this.dispatcher.buscarDisponible(empl);
		}
       try{
    	   System.out.println("*******ejecutando idllamada:"+ String.valueOf(idllamada)+" empleado:"+empl.getNombreEmpleado()+" Estado:"+empl.getDisponible());   
    	   Thread.sleep(delay);
    	   empl.setDisponible(true);
    	   System.out.println("***********fin idllamada:"+ String.valueOf(idllamada)+" empleado:"+empl.getNombreEmpleado()+" Estado:"+empl.getDisponible());
    	   
       }catch(Exception e){   
    	   e.printStackTrace();
    	   System.out.println("idllamada "+idllamada+" empleado "+empl.getNombreEmpleado()+e.getMessage());
       }		
	}

	public int getIdllamada() {
		return idllamada;
	}

	public void setIdllamada(int idllamada) {
		this.idllamada = idllamada;
	}
   
}
